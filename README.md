# Mega and Epics Pulse Generation

## Index  
[Introduction](#Introduction)  <br>
[Set-Up](#Set Up) <br>
[Results](#Results) <br>
[Mega Code](#Mega Code) <br>
[EPICs Code](#EPICs Code) <br>
[Other Notes](#Other Notes) <br>

<a name="Introduction"/><br>
## Introduction


The project creates a led pulse and measures it with a photo detector. For acquisition a mega is used, which communicates with a raspberry using epics stream and asyn modules.

For arduino/mega the response time for the AnalogeRead is ~100 us. Leading to acquisitions of ~10 kHz.

<a name="Set Up"/><br>
## Set Up

For this test a mega connected to a led for creating a pulse and to a photoresistor for analog reading is used.

For the raspberry it is configured to set an acquisition time, where the led pulse is triggered. Then the statistics of the pulse are calculated using a epics subroutine.

Finally, a css opi is configured for interactive operation.

<figure>
<img src="doc/Setup.jpg" alt="alt text" width="250">
  <figcaption><b>Picture showing a Mega board connected to a led and a photoresistor.
</b></figcaption>
</figure>

<a name="Results"/><br>
## Results

Configuring the pulse it is possible to observe that the characteristic time for the system (led plus detector) is around 10 ms.

It is interesting that the pulse intensity is done through PWM modulation with a frequency of 500 Hz. If the acquisition is fast enough, it is possible to observe directly the PWM modulation when the led intensity is lowered.

With this test an adquisition as fast as ~100 us or ~10 kHz can be obtained. This adquisition rate can be useful for pulses of ~1 ms, getting an number of samples that can be representative of the pulse.

For faster acquisition system the AnalogueRead library of arduino should be substituted by overclocking and direct pin assignment.


<figure>
<img src="doc/Result_100ms.png" width="250">
<figcaption><b>Pulse from 10 to 50 ms. The characteristic time is around 10 ms.</b></figcaption>

<img src="doc/Result_10ms.png" width="250">
<figcaption><b>Pulse from 1 to 5 ms at maximum intensity where no PWM is observed.</b></figcaption>

<img src="doc/Result_10ms_max.png" width="250">
<figcaption><b> Pulse from 1 to 5 ms at reduced intensity where PWM can be observed.</b></figcaption>

</figure>


<a name="Mega Code"/><br>
## Mega Code

The mega code is included in `mega/mega.ino`. 
The pulse is generated with the code `P {Adq. Time} {Intensity} {Pulse Beginning} {Pulse End}` 
Serial communication can be done with `pyserial.py`.

<a name="EPICs Code"/><br>
## EPICs code

The EPICs code is based on SMollow tutorial  http://www.smolloy.com/category/arduino/

Details on EPICs installation and creation of an app are in [README_EPICS.md](README_EPICS.md)

The db and proto files and in `appApp/Db/`.
The calculation of statistics for the waveform pulse is implemented in `appApp/src/waveform.cpp`.



<a name="Other Notes"/><br>
## Other Notes

For faster analog read with the Mega board is necessary to change the prescaler(https://www.gammon.com.au/adc) 

The best performance for preescale 16 is around 10 microns.
For faster digital read/write do port manipulation (https://www.arduino.cc/en/Reference/PortManipulation)

The best performance is digital write at around 150 ns



