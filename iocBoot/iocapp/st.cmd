#!../../bin/linux-arm/app

#- You may have to change app to something else
#- everywhere it appears in this file

< envPaths
epicsEnvSet(STREAM_PROTOCOL_PATH,"appApp/Db")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/app.dbd"
app_registerRecordDeviceDriver pdbbase


drvAsynSerialPortConfigure("SERIALPORT","/dev/ttyACM1",0,0,0)
asynSetOption("SERIALPORT",-1,"baud","9600")
asynSetOption("SERIALPORT",-1,"bits","8")
asynSetOption("SERIALPORT",-1,"parity","none")
asynSetOption("SERIALPORT",-1,"stop","1")
asynSetOption("SERIALPORT",-1,"clocal","Y")
asynSetOption("SERIALPORT",-1,"crtscts","N")

cd "appApp/Db"
dbLoadRecords("app.db", "SS='wfm', NELM=100, PORT=SERIALPORT")

cd "${TOP}"

iocInit

## Start any sequence programs
#seq sncxxx,"user=angel"
