# EPICs Notes

For details on installation follow [https://www.smolloy.com/2016/12/epics-ioc-on-a-raspberry-pi/ https://www.smolloy.com/2016/12/epics-ioc-on-a-raspberry-pi/]

# Installation 

## Preparation 

Get epics and modules (asyn, stream), and unpack.
```
   tar -zxf ~/Downloads/baseR3.14.12.5.tar.gz -C ~/epics/
   tar -zxf ~/Downloads/asyn*tar.gz -C ~/epics/
   tar -zxf ~/Downloads/stream* -C ~/epics/ 
```

Do links to usr local

```
   sudo ln -s /home/pi/epics /usr/local/
   ln -s /home/pi/epics/base-3.14.12.5 /home/pi/epics/base
   ln -s /home/pi/epics/asyn* /home/pi/epics/asyn
   ln -s /home/pi/epics/stream* /home/pi/epics/stream
```

Edit `bashrc`:

```
  export EPICS_ROOT=/usr/local/epics
  export EPICS_BASE=${EPICS_ROOT}/base
  export EPICS_HOST_ARCH=`${EPICS_BASE}/startup/EpicsHostArch`
  export EPICS_BASE_BIN=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}
  export EPICS_BASE_LIB=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}
  if [ "" = "${LD_LIBRARY_PATH}" ]; then
      export LD_LIBRARY_PATH=${EPICS_BASE_LIB}
  else
      export LD_LIBRARY_PATH=${EPICS_BASE_LIB}:${LD_LIBRARY_PATH}
  fi
  export PATH=${PATH}:${EPICS_BASE_BIN}
```
    
## Install Base
```
  cd /home/pi/base

   #sudo apt-get install libreadline-dev # May be necessary
  make
```

## Install Asyn 

Edit asyn `configure/RELEASE`:
  ```
  EPICS_BASE=/usr/local/epics/base
  #Comment SEQ line #SNCSEQ=$(SUPPORT)/seq-2-2-3. in case there is compilation problem for IPServer
 ```

Then:
```
  cd /home/pi/epics/ayn
  make
```

## Install Stream 

Edit stream `configure/RELEASE`:
```
  EPICS_BASE=/usr/local/epics/base
  ASYN=/usr/local/epics/modules/asyn
```

then:

```
cd /home/pi/epics/stream
makeBaseApp.pl -t support
make
```

# Create App 

Create app folder
```
    mkdir appfolder
    cd appfolder 
    makeBaseApp.pl -t ioc appname # create application directories
    makeBaseApp.pl -i -t ioc appname # create ioc boot directories
```

Add Asyn and Stream modules
```
    ASYN=/usr/local/epics/modules/asyn
    STREAM=/usr/local/epics/modules/stream
    DEVGPIO=/usr/local/epics/modules/devgpio #If direct access to Raspi GPIO is desired
```

Add db and protos in `app/db` folder

Add cpp and dbds in `app/src` folder

Add dbd, cpp and libs in `app/src/Makefile`

Edit the `cmd` folder then `make`

## Subroutines
For subroutines follow:
https://www.smolloy.com/2016/07/hello-world-for-epics/

Other PVs are accessed in the subroutine by declaring INPA, INPB in the *db and access in the *cpp with arrow operator precord->a, precord->b

## Start-up

For ioc startup execute screen and later detach ctrl a + d

For ioc startup:
  ```./bin/linux-arm/app iocBoot/iocapp/st.cmd```

For boot start-up used `procServ`, add to `rc.local`:

```
cd /home/angel/epics/ioc/raspi_uno/iocBoot/iocapp/ &&  procServ -n "raspi_uno" -i ^D^C 20000 ./st.cmd
```

then for connecting use:
  ```telnet localhost 20000```

# Archiver 
Download and install.

When startup execute "sudo samplesStartup.sh start"

For startup at boot add to `rc.local`

The settings (ip) are in `appliances.xml`
