//Code based on
//http://www.smolloy.com/2015/12/serial-control-of-an-arduino/

#define ADQ_N 100

// Pins deffinition
const int ledPin = 2;
const int sensorPin= A0;

// Read variables
char lastRecvd;
String input = ""; 

//Program Variables
int sensorval = 0;
int ledState = 0;

// Pulse variables
char *pch;
int pulse[ADQ_N];
unsigned long tt[ADQ_N];
int pulse_int;
unsigned long adq_time;
unsigned long adq_t=0;
unsigned long adq_t0;


unsigned long pulse_t0;
unsigned long pulse_t1;

float dt;
int i = 0;


void setup() {

    pinMode(ledPin, OUTPUT);
    pinMode(sensorPin, INPUT);
    Serial.begin(9600); // Make sure the Serial commands match this baud rate

 }

void loop () {
 delay(100);
 while (Serial.available()>0){
    lastRecvd = Serial.read();
    
    if (lastRecvd == '\n'){

        switch(input[0]){
             case 'P':
                 
                pch = strtok (input.c_str() ," "); //reads P

                pch = strtok (NULL ," "); //pulse duration
                adq_time = atol(pch) ;

                pch = strtok (NULL ," "); //pulse intensity
                pulse_int = constrain( atoi( pch ), 0 , 255) ;

                pch = strtok (NULL ," "); //
                pulse_t0 =  atol( pch ) ;

                pch = strtok (NULL ," "); //
                pulse_t1 =  atol( pch ) ;

                dt = (float)(adq_time/ADQ_N);

                analogWrite(ledPin, 0);
                bool bpulse = false;
                adq_t = 0;
                adq_t0 = micros(); // for pulse duration
                i = 0;

                while(adq_t < adq_time ){
                    adq_t = micros() - adq_t0; 

                    //Pulse Writing
                    if ((adq_t > pulse_t0) and (adq_t < pulse_t1 )) {
                        if ( not(bpulse) ) { 
                          analogWrite(ledPin, pulse_int);
                          bpulse = true;}  }      
                    else {if (bpulse) {
                      bpulse = false;
                      analogWrite(ledPin, 0);}}
                    

                    if ( (float)(i*dt) < (float)adq_t){                                    
                      pulse[i]= analogRead(sensorPin); // Takes around 100 us
                      tt[i] = adq_t;
                      i++;}
                      //if (i == ADQ_N){i=0;}
                      
                 }//while
                
                analogWrite(ledPin, 0);

                // Serial Output
                Serial.print("T ");
                for  ( i =0; i<ADQ_N;i++ ){ // write pulse
                    Serial.print( tt[i] ) ;
                    Serial.print( " " ) ;   
                }
                Serial.println();

                Serial.print("P ");
                for  ( i =0; i<ADQ_N;i++ ){ // write pulse
                    Serial.print( pulse[i] ) ;
                    Serial.print( " " ) ;   
                }
                Serial.println();

                // Clear arrays for next pulse
                for  ( i =0; i<ADQ_N;i++ ){ 
                    pulse[i] = 0;
                    tt[i] = 0;
                }//for

                input = ""; 

                break; 
            default:
               input = ""; 
               break;}//switch
           
     }else { // Input is still coming in
        input += lastRecvd;}

  }//while
}//loop
