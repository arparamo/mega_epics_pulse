#include <stdio.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <math.h>

static float wfm_stats(aSubRecord *prec) {

    float *wfm;
    float *time;
    float max=-1E12;
    float min=1E12;
    float sum=0;
    float mean=0;
    float std=0;
    int count=0;

    float *roi_t0=0;
    float *roi_t1=0;
    unsigned int i;

    wfm = (float *)prec->a;
    time = (float *)prec->b;
    roi_t0 = (float *)prec->c;
    roi_t1 = (float *)prec->d;

    for (i=0; i<prec->noa;i++){
        if ( ( time[i] >= roi_t0[0] )  and ( time[i] <= roi_t1[0] ) ){
            if (wfm[i] > max){ max = wfm[i]; }
            if (wfm[i] < min){ min = wfm[i]; }
            sum = sum + wfm[i];
            std = std + wfm[i]*wfm[i];
            count++;
       }//if
    }
    mean = sum/count;
    std = std/count - mean*mean;
    std = sqrtf( std );

    *(float *)prec->vala = max ;
    *(float *)prec->valb = min ;
    *(float *)prec->valc = sum ;
    *(float *)prec->vald = mean ;
    *(float *)prec->vale = std ;

    return 0;
}

/* Note the function must be registered at the end!*/
epicsRegisterFunction(wfm_stats);
